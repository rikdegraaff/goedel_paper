\section{What Lead to Gödel's Paper}

In 1878, the mathematician Georg Cantor proposed the \textit{Continuum Hypothesis} (CH).
CH states that there is no set whose cardinality is strictly between that of the integers and the real numbers \citep{dauben1979georg}.
Cantor would not see his hypothesis resolved within his lifetime.
His fruitless attempts to prove his own hypothesis caused him much anxiety \citep[p. 248]{dauben1979georg}.
\par
As \citet{godel1931} alludes to in his introduction, the decades before this paper were published were marked by a strive towards greater exactness in mathematics.
The goal was to discover all important unsolved problems like the Continuum Hypothesis, and solve them.
There are three mathematicians who produced seminal work during these few decades which had a considerable direct influence on Gödel's paper.
\par
Each work we will discuss \textemdash including Gödel's \textemdash used its own notation, many of which have since fallen out of use or will seem foreign to the target audience of this paper.
We naturally attempt to unify the notation into a more familiar and modern one. We lean heavily on a translation of Gödel's paper from German to English \citep{hirzel2000} in this regard.

\subsection{Peano's Arithmetic}

In 1889, Giuseppe Peano published his book \textit{The principles of arithmetic, presented by a new method} \citep{peano1889} in which he detailed an axiomization of mathematics. \citep[p.83-97]{fregetogodel}
His aim was to create a formal system which models everything of interest to mathematics, ranging from natural numbers to sets, fractions and functions.
He also built upon previous attempts to formalize logic and innovatively separated logical and mathematical operators.
Much of his work is no longer considered relevant today and was criticized by his contemporaries because his system lacked rules of derivation and his proofs instead relied on an intuitive understanding of arithmetic logical operations.
Despite its shortcomings, two things Peano introduced proved to be incredibly influential.
The first was much of the notation he used\footnote{Though, ironically, we will not be using his notation in this paper.}.
More importantly, his axiomization of natural numbers, which consisted of nine axioms \textemdash or fundamental assumptions \textemdash and treated numbers as individuals and the successor relation as an operator whose semantics follow from the axioms, proved especially robust.
Four of the axioms are no longer considered to be axioms of natural numbers and rather of the underlying logic and two further axioms are not necessary for our purposes since \textemdash unlike in Peano's book \textemdash natural numbers will be the only individuals we deal with.
The remaining three put into words are as follows:

\begin{enumerate}
    \item No natural number has 0 as its successor.\footnote{Peano used 1 as the first natural number, not 0.}
    \item If the successors of two numbers are equal, so are the two numbers.
    \item If a set contains 0 and the set containing a number implies that the set also contains its successor, this set contains all natural numbers.
\end{enumerate}

The last axiom is sometimes referred to as the axiom of induction since it allows proofs by induction over the natural numbers.
\par
At the time there was somewhat of a consensus that Peano's Axioms successfully modelled all there is to natural numbers.
Among the proponents was prominent mathematician and logician Bertrand Russell \citep[p. 137]{weitz1952bertrand}.

\subsection{Russell's Principia Mathematica}

In 1903, Bertrand Russell published his book \textit{The principles of Mathematics} \citep{russell1903} in which he reported on the advances made in mathematics by Cantor and Peano among others and presented his now famous paradox which he had discovered two years prior.
The paradox can be paraphrased as: "Consider the set which contains all sets which do not contain themselves. The inclusion of this set in itself implies its exclusion and vice versa."
The implication of this paradox is that any system which allows the definition of such a set (or an equivalent construct) is necessarily \textit{inconsistent} or \textit{incomplete}.
That is to say, there exists a statement such that the statement and its negation are both provable or unprovable respectively.
\\
Russell set out to resolve this paradox by developing a \textit{theory of types} which enforces a strict type on sets and only allows sets to contains sets with a type lower than its own.
Along with some other newly introduced concepts, this was published in a three volume work, \textit{Principia Mathematica}.\citep{principia1, principia2, principia3}
In it Russell defines the formal system PM, which aims to be a minimal axiomization of all of mathematics.
PM was a significant improvement over Peano's axiomization since it had fewer unnecessary axioms and included \textit{rules of derivation} which provided a well defined way to prove statements.
The hope was that removing intuition from mathematical proofs would also provide a principled way to deal with paradoxes.
The minimal axiomization came at the cost of brevity, explaining the 1998 page count.
Infamously, the proof for $1 + 1 = 2$ appears only hundreds of pages into the first volume.

\subsection{Hilbert's Problems}

With several approaches to alleviate Russell's paradox and related paradoxes having been developed and several promising attempts at formalizing all of mathematics being worked on,
there was a sense that mathematics was almost done. All that would be needed is a few more brilliant ideas to identify axioms that need to be added to or removed from systems.
After that mathematics would become grunt work, requiring only to methodically apply rule of derivation to prove all interesting statements about mathematics.
It was in this spirit that David Hilbert had published 23 open questions in mathematics a few years earlier. \citep{hilbert1902}
\\
The problems ranged from very specific and precise (18.b: What is the densest sphere packing?) to very vague (23: Further development of the calculus of variations.).
The first problem on Hilbert's list was unsurprisingly the Continuum Hypothesis. The second is of special interest to us, it simply requests to prove that the axioms of arithmetic are consistent.
