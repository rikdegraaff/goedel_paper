\newcommand{\undersetleq}[3]{\underset{#2 \leq #3}{#1}\,}
\newcommand{\argminleq}{\undersetleq{\func{argmin}}}
\newcommand{\undersetrightleq}[3]{\undersetleq{#1}{#2}{#3} #2}
\newcommand{\existsleq}{\undersetrightleq{\exists}}
\newcommand{\forallleq}{\undersetrightleq{\forall}}
\newcommand{\func}{\textsc}

\section{Gödel's Undecidability Theorem}

As a response to Hilbert's second problem, \citet{godel1931} showed that a wide class of formal systems,
including any reasonable extension of system P and ZF \textemdash another popular formal system at the time \textemdash are necessarily either incomplete or inconsistent.
(Gödel's original claim was actually slightly weaker, but \citet{rosser1936} improved Gödel's theorem resulting in the form as it is most often expressed today.)
Furthermore, he also showed that any such system cannot prove its own consistency or the consistency of any system that is more powerful than it.
\par
In this section, we will provide an overview of Gödel's proof for his first incompleteness theorem for system P and its extensions specifically. We will not aim to achieve the same rigor and completeness as Gödel, 
but rather to give the reader a feel for the tactics employed in the proof as well as the confidence that they understand how the gaps in this overview could be filled to formulate the entire proof.

\subsection{System P}

For the purposes of Gödel's proof, he dealt with a subset of PM combined with the Peano axioms, which we will refer to as \textit{system P}.
The fundamental, undefined symbols of system P are:

\renewcommand{\implies}{\longrightarrow}
\newcommand{\biimplies}{\longleftrightarrow}

\begin{itemize}
    \item $0$ (zero), $S$ (the successor function), $\neg$ (negation), $\lor$ (disjunction), $\forall$ (for all), $($, $)$
    \item $x^m_n$ with $m,n \geq 1$ (a variable of type $n$)
\end{itemize}

We permit ourselves the following shortcuts for variables: $x_i$, $y_i$, $\dotsc$ as a stand-in for any unspecified variable of type $i$ and $x$, $y$, $\dotsc$ as any unspecified variable of type 1.
\\
The notion of a \textit{sign of type $n$} is defined as follows. $0$ is a sign of type 1 and $x^m_n$ is a sign of type $n$. If $p$ is a sign of type 1, then so is $Sp$.
An \textit{elementary formula} has the form $p(q)$ ($p$ contains $q$) where $p$ is a sign of type $n+1$ and $q$ a sign of type $n$.
The set of \textit{well-formed formulas} in system P is the smallest set which contains all elementary formulas and contains $\neg(\phi)$, $(\phi)\lor(\psi)$ and $\forall x (\phi)$ if $\phi$ and $\psi$ are well-formed formulas and $x$ is a variable.
We will leave away parentheses following the common conventions. For quantifiers we shall write $\forall x:$ when we do.
We also introduce the  following abbreviations:
\begin{itemize}
    \item $\phi \land \psi \coloneqq \neg (\neg \phi \lor \neg \psi)$
    \item $\phi \implies \psi \coloneqq \neg \phi \lor \psi$
    \item $\phi \biimplies \psi \coloneqq (\phi \implies \psi) \land (\psi \implies \phi)$
    \item $\exists x: \phi \coloneqq \neg \forall x: \neg \phi$
    \item $x_n = y_n \coloneqq \forall x_{n+1} (x_{n+1}(x_n) \biimplies x_{n+1}(y_n))$
\end{itemize}
System P has the following axioms:
\begin{itemize}
    \item The Peano axioms.
    \begin{enumerate}
        \item $\neg (Sx = 0)$,
        \item $Sx = Sy \implies x = y$,
        \item $\left(x_2(0) \land \forall x: x_2(x) \implies x_2(Sx)\right) \implies \forall y: x_2(y)$.
    \end{enumerate}
    \item The proposition axioms.\\
    Given any well-formed formulas $\phi$, $\psi$ and $\chi$ the following are axioms:
    \begin{enumerate}
        \item $\phi \lor \phi \implies \phi$
        \item $\phi \lor \psi \implies \phi$
        \item $\phi \lor \psi \implies \psi \lor \phi$
        \item $(\phi \implies \psi) \implies (\phi \lor \chi \implies \psi \lor \chi)$
    \end{enumerate}
    The proposition axioms encode semantics of the logical operator $\lor$.
    \item The quantor axioms\\
    Given any formula $\phi$, variable $v_n$, formula where $v_n$ does not occur freely $\psi$ and a sign $c$ of type $n$,
    the following are axioms:
    \begin{enumerate}
        \item $\forall v_n (\phi) \implies \phi_{c \leftarrow v_n}$ where $\phi_{c \leftarrow v_n}$ denotes the formula obtained when every free occurrence of $v_n$ in $\phi$ is replaced by $c$
        \item $\forall v_n (\phi) \lor \psi \implies \forall v_n (\phi) \lor \psi$
    \end{enumerate}
    The quantor axioms encode the intuitive meaning of the universal quantifier $forall$.
    \item The reducability axiom\\
    Given any variables $v_n$ and $u_{n+1}$ and the formula $\phi$ where $u_{n+1}$ does not occur freely, the following is an axiom:
    \begin{enumerate}
        \item $\exists u_{n+1} (\forall v_n (u_{n+1}(v_n)) \biimplies \phi)$
    \end{enumerate}
    The reducability axiom can be interpreted as: for each formula with one free variable, there exists a set which contains exactly those elements for which the formula holds.
    \item The set axiom\\
    Given any variables $x_n$, $x_{n+1}$ and $y_{n+1}$, the following is an axiom:
    \begin{enumerate}
        \item $(\forall x_n (x_{n+1}(x_n) \biimplies y_{n+1}(x_n))) \implies x_{n+1} = y_{n+1}$
    \end{enumerate}
    We call a formula which is identical to another formula with the exception of all variables which are all of type $n$ higher a \textit{type lift} of the other formula.
    The set axiom can be interpreted as: a set is defined entirely by its elements.
\end{itemize}

In order to now define the set of all \textit{provable formulas} of system P, we introduce the rules of derivation for system P: \textit{immediate consequence}.
We say a formula $\phi$ is the immediate consequence of the formulas $\psi$ and $\chi$ if 
$$\chi \text{is} \psi \implies \phi$$
or
$$\phi \text{is} \forall v: \psi$$.
The set of \textit{provable formulas} is the smallest set containing every axiom which is closed under the relationship immediate consequence.

\subsection{Gödel Numbering}

The overall strategy of the proof is to construct as statement within system P which asserts its own unprovability.
Any proof of such a statement would simultaneously demonstrate that the assertion that it is unprovable is false.
Any proof of its negation would mean that the original statement must be provable too, which would make the formal system inconsistent.
Thus, the only options are that the system is inconsistent or incomplete.
\par
In order to construct such a statement, we must enable system P to refer to itself. 
The key component is a mapping from statements within the system to whole numbers. We call such a mapping a \textit{Gödel numbering}.

\newcommand{\GN}[1]{\#(#1)}
\newcommand{\GNl}[1]{\GN{\text{'}#1\text{'}}}

\begin{definition}
The Gödel number of the primitive symbols are \\
$
\GNl{0} = 1,\ 
\GNl{S} = 3,\ 
\GNl{\neg} = 5,\ 
\GNl{\lor} = 7,\\
\GNl{\forall} = 9,\ 
\GNl{(} = 11,\ 
\GNl{)} = 13 \\
\GNl{x^m_n} = p^n \text{where $p$ is the $m$th prime larger than 13 and $n > 0$}
$\\
and the Gödel number of a sequence as\\
$
\GN{\langle a_1, a_2, \dotsc, a_n \rangle} = \prod\limits_{k \leq n} p_k^{\GN(a_k)} \text{where $p_k$ is the $k$th prime.}
$
\end{definition}

This ensures that every primitive sign, formula and sequence of formulas of system P has a unique Gödel number.
For instance, the first Peano axiom $\neg (Sx_1 = 0)$, which is an abbreviation for $\neg \forall x^1_2 ( \neg ( \neg (\neg x^1_2(Sx^1_1) \lor x^1_2(0)) \lor \neg (\neg x^1_2(0) \lor x^1_2(Sx^1_1))))$
has the Gödel number $2^{\GNl{\neg}} \cdot 3^{\GNl{\forall}} \cdot \dotsc \cdot 149^{\GNl{)}} = 2^5 \cdot 3^9 \cdot \dotsc \cdot 151^{13}$

\subsection{Translating statements about system P into statements within system P}

The next step is to construct statements within system P which are equivalent to statements about the system.
As we have seen, the canonical formulas for even simple statements are quite lengthy.
To prevent having to write out these long formulas, \citet{godel1931} used \textit{primitive recursive} functions.

\begin{definition}
Constant functions and the successor function $succ(x) = x + 1$ are primitive recursive functions.\\
The function $f$ defined as
\begin{align*}
f(0, x_2, x_3, \dotsc, x_n) &\coloneqq g(x_2, x_3, \dotsc, x_n)\\
f(k + 1, x_2, x_3, \dotsc, n) &\coloneqq h(f(k, x_2, x_3, \dotsc, x_n), x_2, x_3, \dotsc, x_n)
\end{align*}
is a primitive recursive function if both $g$ and $h$ are primitive recursive functions.\\
Any function $f(x_1, x_2, \dotsc, x_n) \coloneqq g(h_1(x_1, x_2, \dotsc, x_n), \dotsc, h_2(x_1, x_2, \dotsc, x_n))$ is primitive recursive if $g$ and $h_i$ are.
\end{definition}

We further call an $n$-ary relation $R$ primitive recursive if and only if there exists a primitive recursive function $f$ such that $(x_1, x_2, \dotsc, x_n) \in R$ if and only if $f(x_1, x_2, \dotsc, x_n) = 0$.
We also write $R(x_1, x_2, \dotsc, x_n)$.\\

In the original paper, 46 relations and functions between numbers which are equivalent to various statements about system P are built up.
With the exception of the last relation, they are constructed using rules which are proven to yield primitive recursive relations.
For the purpose of this paper, it will suffice to merely list those relations which we consider of particular importance or which serve as an illustrative example of the more interesting methods employed in constructing the full set of 46 relations.
We will call attention to any new rules of construction that are used and provide a brief argumentation as to why they produce primitive recursive relations.
\\
$$
\func{div}(x, y) \equiv \existsleq{z}{x}: x = y \cdot z
$$
This relation holds if and only if x is divisible by y.
Since multiplication is repeated addition which, in turn, is repeated application of the successor function, it is obvious that multiplication is primitive recursive.
What is more interesting is $\existsleq{z}{x}$ which should be read as a bounded existential quantifier, and not simply as an existential quantifier and a separate constraint on $z$.
It is possible to define a primitive recursive function $\upchi$ such that $\upchi(x, \overrightarrow{z}) = 0$ if for all $y \leq x$ the primitive relation $R(y, \overrightarrow{z})$ does not hold and $\upchi(x, \overrightarrow{z}) = y$ where $y$ is the smallest natural number for which the relation does hold otherwise.
The function is 0 if $x$ is 0. For any larger $x$, it is $\upchi(x-1, \overrightarrow{z})$ if that is larger than 0 or the relation doesn't hold, $x$ if the relation does hold and 0 otherwise.
In order to obtain an existential quantifier from this, we simply need to instantiate this function $\upchi$ with some value which is larger than the lowest $y$ for which the relation holds.
This is why we can only construct a bounded existential quantifier, since we can use the bound as an input for $\upchi$.
\\
\begin{align*}
\func{item}(n, x) \coloneqq &\argminleq{y}{x} \func{div}(x, \func{primeFactor}(n, x)^y)\\
    &\land \neg \func{div}(x, \func{primeFactor}(n, x)^{y+1})
\end{align*}
This function returns the Gödel number of the $n$th item in the sequence $s$ such that $\GN{s} = x$.
Note that this sequence can be both a formula or a sequence of formulas.
$\argminleq{y}{x}$ is taken to mean the smallest $y$, smaller than or equal to $x$ for which the following relation holds and 0 if no such $y$ exists.
$\func{primeFactor}(n, x)$ is the $n$th smallest prime factor of $x$.
This function can be constructed similarly to the bounded existential quantifier.
\\
\begin{align*}
x \circ y \coloneqq \argminleq{z}{\func{prime}(|x| + |y|)^{x+y}} 
    &\left(\forallleq{n}{|x|} : \func{item}(n, z) = \func{item}(n, x)\right)\\
    \land &\left(\forallleq{n}{|y|} : \func{item}(n + |x|, z) = \func{item}(n, y) \lor n = 0\right)
\end{align*}
Given $\GN{s}$ and $\GN{t}$ as inputs, this function is $\GN{st}$, where $st$ is the concatenation of the sequences $s$ and $t$.
$|x|$ here refers the length of the sequence $s$ such that $\GN{s} = x$, which can be defined similarly to $\func{item}(n,x)$.
$\func{prime}(n)$ is the $n$th prime number. 
This function provides a great example of how flexible bounded versions of quantifiers can be despite their restriction.
Since $z$ is the Gödel number of the concatenation, it will be expressible as 
\begin{align*}
2^{n_1} \cdot 3^{n_2} \cdot \dotsc \cdot p_{|x|+|y|}^{n_{|x|+|y|}}
    &= \prod\limits_{1 \leq k \leq |x| + |y|} p_k^{n_k}\\
    &\leq \prod\limits_{1 \leq k \leq |x| + |y|} p_k^{\max\limits_i n_i}\\
    &< p_{|x|+|y|}^{(|x|+|y|) \cdot \max\limits_i n_i}\\
    &< p_{|x|+|y|}^{x+y}
\end{align*}
Clearly, we could have found a lower bound, but the resulting function would have been harder to understand.
\\
$$
\func{vtype}(n,x) \equiv \existsleq{z}{x}: \func{isPrime}(z) \land x = z^n \land z > 13 \land n \neq 0
$$
This relation holds if x is the Gödel number of a variable of type $n$, i.e. $x = \GNl{x^m_n}$ for some $m$.
\\
\begin{align*}
\func{succ}(0,x) &\coloneqq x\\
\func{succ}(n+1,x) &\coloneqq \func{seq}\left(\GNl{S}\right) \circ \func{succ}(n,x)
\end{align*}
In this translation of the successor function, we see an application of the recursion scheme in the definition of primitive recursive functions,
where a function is permitted to recursively depend upon itself provided one of its arguments in the recursive call is decreased.
$\func{seq}(x)$ is simply $2^x$ and thus returns the Gödel number of the singleton sequence which contains only $s$ such that $\GN{s} = x$.
\par
We can also use this successor function to define a function which transforms a number into the Gödel number of that number's representation in system P:
\begin{align*}
\func{num}(n) \coloneqq \func{succ}\left(n,\GNl{0}\right)
\end{align*}
\\
\begin{align*}
\func{type}(n,x) \equiv 
    \Bigl( 
        n = 1 \land &\existsleq{m,n}{x} : \left(m = \GNl{0} \lor \func{vtype}\left( 1,m \right) \right)\\
        &\quad \land x = \func{succ}(n, \func{seq}(m))
         \Bigr)\\
    \lor \Bigl( n > 1 \land &\existsleq{v}{x}: \func{vtype}(n,v) \land x = \func{seq}(v) \Bigr)
\end{align*}
$\func{type}(n,x)$ holds if and only if $x$ is the Gödel number of a segment of a formula which represents an object of type $n$.
$\func{paren}(z)$ is the Gödel number of $(\phi)$ if $\GN{\phi} = z$.
\\
\begin{align*}
\func{elFm}(x) \equiv \existsleq{y,z,n}{x}: \func{type}(n+1,y) \land \func{type}(n,z) \land x = y \circ \func{paren}(z)
\end{align*}
Holds if and only if $x$ is the Gödel number of an elementary formula like $x_{n+1}(x_n)$ or $x_2(SS0)$.
\\
\begin{align*}
\func{fmSeq}(s) \equiv |s| > 0 \land &\forallleq{n}{|s|}( n = 0 \lor \func{elFm}(\func{item}(n,s))\\
    &\lor \existsleq{p,q}{n}( \func{op}(\func{item}(n,s),\func{item}(p,s),\func{item}(q,s))\\
    &\quad \land p > 0 \land q > 0))
\end{align*}
This relation holds if and only if $s$ is Gödel number of a sequence of Gödel numbers of formulas which are either elementary formulas or a logical operator applied to at most two formulas that appeared earlier in the sequence.
$\func{op}(x, y, z)$ holds if and only if $x$ is the Gödel number of a formula obtained by applying $\forall$, $\lor$ or $\neg$ to the formulas $y$ and $z$ are the Gödel numbers of.
As a consequence, we know that every formula in the sequence $s$ is well-formed. 
Note that $\func{item}(n,s)$ in this case does not refer to the Gödel number of a primitive symbol, but rather a formula.
Likewise, $s$ is the Gödel number of a sequence of formulas, not simply a formula.
\\
\begin{align*}
\func{isFm}(x) \equiv \existsleq{s}{\func{prime}(|x|^2)^{x \cdot |x|^2}}: \func{fmSeq}(s) \land x = \func{lastItem}(s)
\end{align*}
Using $\func{fmSeq}(s)$ we can finally define a relation which holds if and only if $x$ is the Gödel number of a well-formed formula by asserting that $x$ is the last item in such a formula sequence.
\\
\begin{align*}
\func{bound}(v,n,x) \equiv &\func{isVar}(v) \land \func{isFm}(x)\\
    \land &\existsleq{a,b,c}{x}( x = a \circ \func{forall}(v,b) \circ c \land \func{isFm}(b)\\
    &\land |a| + 1 \leq n \leq |a| + |\func{forall}(v,b)|)
\end{align*}
The $\func{bound}$ relation holds if and only if the variable with the Gödel number $v$ is not free at position $n$ in the formula with Gödel number $x$.
It forms the basis for a number of following relations relating to free and bound variables.
$\func{forall}(v, x)$ is the Gödel number of $\forall y (\phi)$ where $v$ is the Gödel number of the variable $y$ and $x$ that of the formula $\phi$.
Intuitively, the relation checks if the formula position $n$ falls within a part of the formula where the variable was bound by a universal quantifier.
\\
\begin{align*}
\func{subst'}(0,x,v,y) &\coloneqq x\\
\func{subst'}(k+1,x,v,y) &\coloneqq \func{insert}(\func{subst'}(k,x,v,y), \func{freePlace}(k,v,x),y)\\
\func{subst}(x,v,y) &\coloneqq \func{subst'}(\func{freePlaces}(v,x),x,v,y)
\end{align*}
$\func{subst}(x,v,y)$ is the Gödel number of a formula where the formula referred to by $y$ is inserted at every position in the formula referred to by $x$ where the variable referred to by $v$ occurs freely.
$\func{subst'}(k, x, v,y)$ is an auxiliary function which does the same as $\func{subst}$ but only inserts the formula for the first $k$ free occurrences of the variable.
$\func{freePlace}$ and $\func{freePlaces}$ are the $k$th free occurrence and the amount of free occurrences of the variable respectively.
$\func{insert}(x, n, y)$ is the Gödel number of a sequence which is equal to the sequence $x$ refers to with the $n$th item replaced by the sequence $y$ is the Gödel number of.
\\
\begin{align*}
\func{peanoAxiom}(x) \equiv x = pa_1 \lor x = pa_2 \lor x = pa_3
\end{align*}
The Peano axioms have specific Gödel numbers, so we can simply precalculate them and check if a Gödel number $x$ is equal to any of them.
\\
\begin{align*}
\func{prop2Axiom}(x) \equiv \existsleq{y,z}{x}: \func{isFm}(y) \land \func{isFm}(z) \land x = \func{impl}(y,\func{or}(y,z))
\end{align*}
This relation holds if $x$ is the Gödel number of a formula which follows from the second proposition axiom\footnote{We defined the relation for the second axiom only because the original paper used the first axiom as an example. There is nothing special about either of the two axioms that would lead us to pick one over the other as an example.}.
More precisely, the formula has the form $p \rightarrow p \lor q$ with $p$ and $q$ being any two well-formed formulas.
$\func{impl}$ and $\func{or}$ are the Gödel numbers of implications and disjunctions respectively.
We can translate the other three proposition axioms analogously.
\\
\begin{align*}
\func{quantor1Axiom}(x) \equiv \existsleq{v,y,z}{n}: 
    &\func{vtype}(n,v) \land \func{type}(n,z) \land \func{isFm}(y)\\
    \land &\func{quantor1AxiomCondition}(z,y,v)\\
    \land &x = \func{impl}(\func{forall}(v,y),\func{subst}(y,v,z))
\end{align*}
In order to define the first quantor axiom, we require an auxiliary relation $\func{quantor1AxiomCondition}(z,y,v)$,
which holds if the formula to be inserted does not contain any free variables which would be bound at any position where the variable to be replaced is free.
Other than that, it is similar to the Peano axioms in that it simply asserts the existence of Gödel numbers which fulfill some necessary conditions
and finally asserts that $x$ is the Gödel number of a specific composition of the formulas or symbols these Gödel numbers refer to.
The second quantor axiom and the reducability axiom can be constructed similarly.
\\
\begin{align*}
\func{setAxiom}(x) \equiv \existsleq{n}{x}: x = \func{typeLift}(n,sa)
\end{align*}
Since the set axiom concerns a specific formula and any type lift of this formula,
we can once again precalculate its Gödel number $sa$.
$\func{typeLift}(n, x)$ can be implemented using an argmin to find the Gödel number of a formula which is identical to the original formula,
but has variables which are $n$ types higher.
\\
This gives us relations which can be used to check if a number is the Gödel number of an axiom of system P; we can move on to proofs.
\\
\begin{align*}
\func{consequence}(x,y,z) \equiv y = \func{impl}(z,x) \lor \existsleq{v}{x}: \func{isVar}(v) \land x = \func{forall}(v,y)
\end{align*}
This relation holds if and only if $x$ is the Gödel number of an immediate consequence of the formulas $y$ and $z$ refer to.
\\
\begin{align*}
\func{isProof}(s) &\equiv |s| > 0\\
    \land &\forallleq{n}{|s|}\Bigl( n = 0 \lor \func{isAxiom}(\func{item}(n,s))\\
    &\lor \existsleq{p,q}{n} \bigl( \func{consequence}(\func{item}(n,s), \func{item}(p,s), \func{item}(n,s))\\
    &\quad \land p > 0 \land q > 0 \bigr) \Bigr)
\end{align*}
$\func{isProof}(s)$ recognizes $s$ as a proof if and only if $s$ is a sequence of Gödel numbers of formulas which are either axioms or the immediate consequence of at most two formulas that appeared earlier in the sequence.
Compare this definition to that of $\func{fmSeq}(s)$.
Elementary formulas have been replaced by axioms and applications of operators have been replaced by the immediate consequence rule, but the structure is identical.
It would be tempting to define $\func{provable}(x)$ analogously to $\func{isFm}(x)$,
but this is where the restriction to bounded quantifiers becomes relevant.
Since no upper bound on the length of the shortest proof for a formula can be derived from the formula,
we cannot assert its existence using a primitive recursive relation.
\\
We can however define the following primitive recursive relation:
\begin{align*}
\func{proves}(s,x) \equiv x = \func{lastItem}(s) \land \func{isProof}(s)
\end{align*}
\\
And although it is not primitive recursive, we can likewise define the relation which holds if and only if a formula is provable within system P:
\begin{align*}
\func{provable}(x) \equiv \exists\,s: \func{proves}(s,x)
\end{align*}

\subsection{The first incompleteness Theorem}

In order to tie provability to primitive recursive relations, \citet{godel1931} proves the following:

\begin{theorem}[\citeauthor{godel1931}, \citeyear{godel1931}]
\label{thm:1}
Given an n-ary primitive recursive relation $R$, there exists a formula in system P with Gödel number $r$ with at most $n$ free variables with Gödel numbers $v_1, \cdots, v_n$ such that for any natural numbers $x_1, \cdots, x_n$
\begin{align*}
\text{if } &R(x_1, \cdots, x_n) \text{ then}\\ 
    &\func{provable}(\func{subst}( \cdots \func{subst}(r, v_1, \func{num}(x_1) \cdots ), v_n, \func{num}(x_n)))\\
    \text{and}\\
\text{if not } &R(x_1, \cdots, x_n) \text{ then}\\
    &\func{provable}(\func{not}( \func{subst}( \cdots \func{subst}(r, v_1, \func{num}(x_1) \cdots ), v_n, \func{num}(x_n)))).
\end{align*}
\end{theorem}

\begin{proofsketch}
This can be proven with induction over the structure of primitive recursive functions.
For the base case of a constant function, the formula can just be any tautology or contradiction depending on whether the function is constant 0 or some other number.\\
Since system P can model recursion and composition of functions, it's easy to convince oneself that the induction step for both schemes can be done.
\end{proofsketch}
We can now dive into the final steps to the proof of Gödel's first incompleteness theory.
To assure that the proof is applicable to more systems besides system P, we define the notion of extensions to system P.
We allow system P to be extended by any primitive recursive set of formulas $\upphi$,
by which we mean a set of formulas whose Gödel numbers are exactly the range of a primitive recursive function.
Note that, while every finite set of formulas is primitive recursive, $\upphi$ does not need to be finite to qualify.
\\
For such an extension of system P, we can define the relations $\func{isProof}_\upphi$, $\func{proves}_\upphi$ and $\func{provable}_\upphi$
analogously to their counterparts for the unextended system P with the exception that an item in a $\upphi$-proof as recognized by $\func{isProof}_\upphi$
does not have to be an axiom or an immediate consequence of two earlier formulas, but can also be an element of $\upphi$.
We call such an extension of system P \textit{$\omega$-consistent} if and only if there is no $x$ and $v$ such that $x = \GN{\phi}$, $\phi \in \upphi$, $v$ is a free variable of $\phi$ and
\begin{align*}
&\func{provable}_\upphi\left(\left( \forall n: \func{subst}(x, \GN{v}, \func{num}(n)) \right) \right)\\ 
\land &\func{provable}_\upphi\left( \func{not}\left( \func{forall}\left( \GN{v}, x \right) \right) \right)
\end{align*}
The notion of $\omega$-consistency is stronger than consistency, since we cannot derive a contradiction from the formula $\phi$ if it exists.
The apparent contradiction only arises within the system when the infinite set of concrete formulas resulting from substituting all natural numbers into $\phi$ are taken into account that the contradiction could be proven\footnote{Hence the use of $\omega$, which is also a common name for the smallest infinite ordinal number.}.
\\
\begin{theorem}[\citeauthor{godel1931}, \citeyear{godel1931}]
For each $\omega$-consistent set of formulas $\upphi$ which is primitive recursive,
there exists a formula with Gödel number $r$ such that neither $\func{provable}_\upphi\left(\func{not}\left(\func{forall}\left(\GNl{x}, r\right)\right)\right)$ nor\\
$\func{provable}_\upphi\left(\func{forall}\left(\GNl{x}, r\right)\right)$ hold.
\end{theorem}
\begin{proofsketch}
Consider the relation
\begin{align}
\func{doesNotProveRecursive}(s,x) \equiv \neg \func{proves}_\upphi(s, \func{subst}(x, \GNl{v}, \func{num}(x))) \label{def:q}
\end{align}
\\
This can be read as the proof referred to by the Gödel number $s$ does not prove the formula with the Gödel number $x$ with $x$ (the number represented in system P) substituted for every free occurence of the variable $v$.
Since this relation is primitive recursive, per Theorem \ref{thm:1}, there exists a corresponding Gödel number $q$ with two free variables for which every combination of $s$ and $x$ for which the relation holds,
the formula obtained from substituting $s$ and $x$ is provable and the negation is provable for combinations of $s$ and $x$ for which the relation does not hold.
\\
We can now define the Gödel numbers
\begin{align}
noProofForRecursive \coloneqq \func{forall}(\GNl{s},q) \label{def:p}
\end{align}
and
\begin{align}
r \coloneqq \func{subst}(q,\GNl{x},\func{num}(noProofForRecursive)) \label{def:r}
\end{align}
$r$ is the Gödel number of a formula with one free variable $s$ which holds if $s$ is not a proof for a formula which asserts no proof exists for it.
\\
When we investigate the formula with Gödel number $\func{forall}(\GNl{s},r)$ we find
\begin{align}
\label{def:far}
\begin{split}
\func{forall}(\GNl{s},r) &= \func{forall}(\GNl{s},\\
    &\quad \func{subst}(q,\GNl{x},\func{num}(noProofForRecursive)))\ \text{(by \ref{def:r})}\\
    &\equiv \func{subst}(\func{forall}(\GNl{s},q),\GNl{x},\\
    &\quad \func{num}(noProofForRecursive)))\\
    &= \func{subst}(noProofForRecursive, \GNl{x},\\
    &\quad \func{num}(noProofForRecursive)\ \text{(by \ref{def:p})}
\end{split}
\end{align}
This tells us that the formula with the Gödel number $\func{forall}(\GNl{s},r)$ is equivalent to 
the formula which \textemdash when the Gödel number of another formula is plugged into it, asserts this formula with its own Gödel number plugged into it cannot be proven \textemdash with its own Gödel number plugged into it.
i.e. \textit{"I cannot be proven"}.
\\
Taking into account \ref{def:q} and \ref{def:far} we get that if there were an $s$ such that\\ $\func{proves}_\upphi(\func{forall}(\GNl{s},r))$, then it is also provable that\\ $\func{provable}_\upphi(\func{not}(\func{subst}(r, \GNl{s}, \func{num}(x)))))$
which by the first quantor axiom implies that the negation of the antecedent is also provable, which would be a contradiction in the system which in turn would contradict the assumption that $\upphi$ is $\omega$-consistent.
\\
Similarly, we get that since $\func{forall}(\GNl{s},r)$ is not provable, there can't be a single $s$ which is a proof for it.
Because of the properties of $q$ as discussed above, this means that for every number $s$ which is not the Gödel number of a valid proof, we can prove that the formula with Gödel number $r$ holds.
Since this must hold for every natural number, the formula with Gödel number $\func{not}({\func{forall}(\GNl{s},r)})$ cannot be provable without resulting in an $\omega$-inconsistent system which contradicts the assumption.
\end{proofsketch}

The consequence of this theorem, as well as other related theorems in Gödel's paper and Rosser's improvement \citep{rosser1936} is that the main goal mathematicians had been striving towards for decades was unattainable.
Any interesting system of arithmatic will necessarily contain statements which can neither be proven nor disproven, or every statement \textemdash including false ones \textemdash can be proven.
It still is widely believed that the former is the case. It was ultimately proven that the Continuum Hypothesis which caused Cantor so much anciety is one such statement which can neither be proven nor disproven.
